<?php

/**
 * @package avto_roko_lestvica
 * @version 0.1
 */

 /*
Plugin Name: Avtomatska rokometna lestvica
Plugin URI: https://bitbucket.org/JSila/avtomatska-rokometna-lestvica
Description: Periodično obišče podane spletne naslove za različne rokometne lige in iz njih izlušči podatke o razvstitvah rokometnih ekip v strukturirani obliki. Vključuje tudi kratko kodo (shortcode) za prikaz podatkov v HTML tabeličnem načinu.
Author: Jernej Sila
Version: 0.1
Author URI: https://www.jernejsila.com
License:     GPLv2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

require_once(__DIR__ . '/lib/WP_Settings.php');

$lestvica_settings = new WP_Settings('lestvica_options');
$lestvica_settings->add_section(null, null)
	->add_field('lestvica_list', 'Podatki o lestvicah', 'lestvica_list_control');


function lestvica_optionally_set_default_data() {
	$lestvica_default_data = "clani,http://livestat.rokometna-zveza.si/#/liga/1155/sezona/64/lestvica
mladinci,http://livestat.rokometna-zveza.si/#/liga/347/sezona/64/lestvica";
	
	if (trim(get_option('lestvica_list')) === "") {
		update_option('lestvica_list', $lestvica_default_data);
	}	
}

function lestvica_list_control() {
	lestvica_optionally_set_default_data();

	?>
	<textarea name="lestvica_list" id="lestvica_list" rows="5" cols="80" wrap="off"><?php echo get_option('lestvica_list'); ?></textarea>
	<p class="description">Vrstica predstavlja podatke za eno lestvico (ključ, spletni naslov lestvice)</p>
	<?php
}

add_action('admin_init', array($lestvica_settings, 'init'));

function lestvica_settings_page() {
    add_options_page(
		'Nastavitve za avtomatsko posodabljanje rokometnih lestvic',
		'Rokometne lestvice',
		'manage_options',
		'lestvica_options',
		'lestvica_page_init'
	);
}

function lestvica_page_init() {
  ?>
  <div class="wrap">
    <h1>Nastavitve za avtomatsko posodabljanje rokometnih lestvic</h1>

	<form method="POST" action="options.php">
		<?php 
			global $lestvica_settings;
			$lestvica_settings->fields();
			$lestvica_settings->do_sections();
			submit_button();
		?>
	</form>
  </div>
  <?php
}

add_action('admin_menu', 'lestvica_settings_page');

add_action('lestvica_update_rankings', 'lestvica_update_func');

register_activation_hook(__FILE__, 'lestvica_activation');
register_deactivation_hook(__FILE__, 'lestvica_deactivation');

function lestvica_activation() {
	lestvica_optionally_set_default_data();

    if (!wp_next_scheduled('lestvica_update_rankings')) {
        wp_schedule_event(strtotime('midnight'), 'daily', 'lestvica_update_rankings');
    }
}

function lestvica_deactivation() {
    wp_clear_scheduled_hook('lestvica_update_rankings');
}

function getRankTableHTML($url, $selection) {
	$rows = fetch_selection_data($url);

    array_unshift($rows, ['Mesto', 'Ekipa', 'Točke']);

    $htmlDoc = new DOMDocument;

	$table = $htmlDoc->createElement('table');
	$htmlDoc->appendChild($table);
	$table->setAttribute('id', "lestvica_{$selection}");
	$table->setAttribute('class', 'lestvica');

	foreach ($rows as $row) {
		$rank    = $row[0];
		$team    = $row[1];
		$points  = $row[2];
		
		$tr = $htmlDoc->createElement('tr');
		if (strpos(strtolower($team), 'trimo') !== false) {
			$tr->setAttribute('style','font-weight:bold');
		}

		$tr->appendChild($htmlDoc->createElement('td', $rank));

		$teamCol = $htmlDoc->createElement('td', $team);
		$teamCol->setAttribute('align', 'left');
		$teamCol->setAttribute('style', 'padding-left:1em');
		$tr->appendChild($teamCol);
	
		$tr->appendChild($htmlDoc->createElement('td', $points));
	
		$table->appendChild($tr);
	}
	
    return $htmlDoc->saveHTML();
}

function fetch_selection_data($url) {
    $segments = explode("/", $url);
    $leagueId = $segments[5];
    $seasonId = $segments[7];

    $response = json_decode(file_get_contents("https://infostat.rokometna-zveza.si/api/public/pregledLige/{$leagueId}/sezona/{$seasonId}"));

    return array_map(function ($team) {
        return [
            $team->stevilka,
            $team->klub,
            $team->tocke,
        ];
    }, $response->data->lestvica);
}

function lestvica_foreach($cb) {
	foreach (explode("\n", get_option('lestvica_list')) as $row) {

		list($selection, $url) = explode(",", $row);

		$cb(trim($selection), trim($url));
	}
}

function lestvica_update_func() {
	lestvica_foreach(function($selection, $url) {
		$html = getRankTableHTML($url, $selection);
		update_option("lestvica_{$selection}", $html);
	});
}

function lestvica_display_func($atts) {
	$selection = $atts['selekcija'];

	return get_option("lestvica_{$selection}");
}

add_shortcode('lestvica', 'lestvica_display_func');