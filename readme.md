# Avtomatska rokometna lestvica

WordPress vtičnik, ki periodično prebere spletno stran s statistiko ligaške rokometne lestvice, izbere samo določene stolpce (uvsrtitev, ime ekipe, točke) in jih shrani v bazo spletne strani, na kateri se potem prikažejo. Posamezniku tako ni potrebno tega početi ročno.

Za bolj podroben in tehničen opis delovanja beri [prispevek](https://www.jernejsila.com/2018/01/29/avtomatska-rokometna-lestvica-wordpress-plugin) na mojem blogu (v angleščini).