<?php

class WP_Settings_Section {
    protected $id = '';
    protected $title = '';
    protected $output_callback = '';
    protected $fields = array();

    public function __construct($id, $title, $output_callback) {
        $this->id = $id;
        $this->title = $title;
        $this->output_callback = $output_callback;
    }

    public function add_field($id, $title, $output_callback) {
        $this->fields[$id] = array(
            'id'              => $id,
            'title'           => $title,
            'output_callback' => $output_callback
        );
        return $this;
    }

    public function get_id() {
        return $this->id;
    }

    public function get_title() {
        return $this->title;
    }

    public function get_output_callback() {
        return $this->output_callback;
    }

    public function get_fields() {
        return $this->fields;
    }
}

class WP_Settings {
    protected $page_id = '';
    protected $sections = array();
    protected $options_group = '';

    public function __construct($page_id) {
        $this->page_id = $page_id;
        $this->options_group = $page_id;
    }

    public function add_section($title = null, $output_callback = null) {
        $section_id = uniqid();
        $section = new WP_Settings_Section($section_id, $title, $output_callback);
        $this->sections[$section_id] = $section;
        return $section;
    }
    
    public function init() {
        foreach ($this->sections as $section_id => $section) {
            add_settings_section(
                $section_id,
                $section->get_title(),
                $section->get_output_callback(),
                $this->page_id
            );
            foreach ($section->get_fields() as $field_id => $field) {
                add_settings_field(
                    $field_id,
                    $field['title'],
                    $field['output_callback'],
                    $this->page_id,
                    $section_id
                );
                register_setting($this->options_group, $field_id);
            }
        }
    }

    public function do_sections() {
        do_settings_sections($this->page_id);
    }
    
    public function fields() {
        settings_fields($this->options_group);
    }
}